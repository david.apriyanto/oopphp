<?php 
require_once ("animal.php");
require_once ("frog.php");
require_once ("ape.php");

//release 0
$sheep = new Animal("shaun");

echo $sheep->name . "<br>"; // "shaun"
echo $sheep->legs . "<br>"; // 2
echo $sheep->cold_blooded . "<br><br>"; // false

//release 1
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

echo "<br>" ;

$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"
?>